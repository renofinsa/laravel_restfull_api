<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meeting;
class MeetingController extends Controller
{
    public function __construct(){
      $this->middleware(
        'jwt.auth',
        ['except' => ['show']]
      );
    }

    public function index()
    {
      $meetings = Meeting::all();
      foreach ($meetings as $key) {
        $key->view_meeting = [
          'href' => 'api/v1/meeting/'.$key->id,
          'method' => 'GET'
        ];
      }

      $response = [
        'message' => 'List of all meetings',
        'value' => $meetings
      ];

      return response()->json($response, 200);
    }

    public function store(Request $request)
    {
        $this->validate($request,[
          'title' => 'required|min:10|unique:meetings',
          'description' => 'required',
          'time' => 'required',
          'user_id' => 'required',
        ]);

        $title = $request->input('title');
        $description = $request->input('description');
        $time = $request->input('time');
        $user_id = $request->input('user_id');

        $meeting = new Meeting([
          'title' => $title,
          'description' => $description,
          'time' => $time
        ]);

        if ($meeting->save()) {
          $meeting->users()->attach($user_id);
          $meeting->view_meeting = [
            'href' => 'api/v1/meeting' . $meeting->id,
            'method' => 'GET'
          ];
          $message = [
            'message' => 'Meeting created',
            'meeting' => $meeting
          ];
          return response()->json($message, 201);
        }


        $response = [
          'message' => 'Error during creation'
        ];

        return response()->json($response, 404);
    }

    public function show($id)
    {
        $meeting = Meeting::with('users')->where('id',$id)->firstOrFail();
        $meeting->view_meetings = [
          'href' => 'api/v1/meeting',
          'method' => 'GET'
        ];

        $response = [
          'message' => 'Meeting information',
          'meeting' => $meeting
        ];
        return response()->json($response, 200);
    }
    public function update(Request $request, $id)
    {
      $this->validate($request,[
        'title' => 'required|min:10|unique:meetings',
        'description' => 'required',
        'time' => 'required',
        'user_id' => 'required',
      ]);

      $title = $request->input('title');
      $description = $request->input('description');
      $time = $request->input('time');
      $user_id = $request->input('user_id');

      $meeting = Meeting::with('users')->FindOrFail($id);

      if (!$meeting->users()->where('users.id',$user_id)->first()) {
        return response()->json(['message' => 'user not registered for meeting, update not successful'], 401);
      }

      $meeting->time = $time;
      $meeting->title= $title;
      $meeting->description= $description;

      if (!$meeting->update()) {
        return response()->json([
          'message' => 'Error during update'
        ], 404);
      }

      $meeting->view_meeting = [
        'href' => 'api/v1/meeting/'.$meeting->id,
        'method' => 'GET'
      ];

      $response = [
        'message' => 'Meeting Updated',
        'value' => $meeting
      ];

      return response()->json($response, 200);

    }

    public function destroy($id)
    {
        $meeting = Meeting::FindOrFail($id);
        $users = $meeting->users;
        $meeting->users()->detach();

        if (!$meeting->delete()) {
          foreach ($users as $key) {
            $meeting->users->attach($key);
          }
           return $response()->json([
            'message' => 'Deletion Failed'
          ], 404);
        }

        $response =[
          'message' => 'Meeting Delete',
          'create' => [
            'href' => 'api/v1/meeting',
            'method' => 'POST',
            'params' => 'title, description, time'
          ]
        ];
        return response()->json($response, 200);

    }
}
